### Background
> Square every digit of a number. For example, if we run 9119 through the function, 811181 will come out, because 9^2 is 81 and 1^2 is 1.

### Challenge
> Note: The function accepts an integer and returns an integer