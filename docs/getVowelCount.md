### Background
> We will consider a, e, i, o, u as vowels (but not y).

### Challenge
> Return the number (count) of vowels in the given string. The input string will only consist of lower case letters and/or spaces.
