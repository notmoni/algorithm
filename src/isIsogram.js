module.exports = function isIsogram(str) {
  if (typeof str !== 'string') return false;

  const string = str.toLowerCase();
  const arr = string.split('');

  // This if statement is for the one case where
  // str = '';
  if (string === '') {
    return true;
  }

  // Assuming we are using only letters the Alphabet
  // has 26 characters. So if the string is more than 26
  // characters there has to be a reoccurring character.
  if (string > 26) return false;

  // Now we have a nested for-loop to check
  // every character with every other character.
  for (let i = 0; i < arr.length; i += 1) {
    for (let a = 0; i < arr.length; a += 1) {
      if (arr[a] === arr[i]) {
        // If the letters are the same return true
        return true;
      }
    }
  }
  // else return false;
  return false;
};
