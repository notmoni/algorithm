module.exports = function squareDigits(num) {
  // We first convert each digit in the number
  // into an array.
  const digits = (`${num}`).split('');

  // Make the answer a blank string.
  let answer = '';

  // Interate throught the array with each digit
  // and find the square for that digit.
  for (let i = 0; i < digits.length; i += 1) {
    //  Then we concatenate it to the blank string.
    answer += `${digits[i] ** 2}`;
  }
  // Before we return the string
  // we parse it into a Integer with
  // the radix of 10.
  return parseInt(answer, 10);
};
