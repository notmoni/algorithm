module.exports = function getVowelCount(str) {
  // Lets say the vowel count start with 0
  let vowelsCount = 0;

  // Make the string to lowercase and trim it to remove any whitespace.
  const string = str.trim().toLowerCase();

  // Make it into a array to compare is each letter is a vowel.
  const arr = string.split('');

  // Make a array of vowels
  const vowelArr = ['a', 'e', 'i', 'o', 'u'];

  // Make a nested for-loop to check each letter
  // to each letter in the vowel array.
  for (let i = 0; i < arr.length; i += 1) {
    for (let a = 0; a < vowelArr.length; a += 1) {
      // If its a match...
      if (vowelArr[a] === arr[i]) {
        // we add 1 to the vowelsCount variable.
        vowelsCount += 1;
      }
    }
  }
  // After the for loop we return the vowel count.
  return vowelsCount;
};
