module.exports = function isDivisible(n, x, y) {
  // Use the modulus Divison and check if they
  // both equal 0, else its not divisible
  if (n % x === 0 && n % y === 0) {
    return true;
  }
  return false;
};
