const assert = require('assert');

const { describe } = require('mocha');
const { it } = require('mocha');

const isIsogram = require('../src/isIsogram');

describe('isIsogram', () => {
  it('test for isogram', () => {
    assert.ok(isIsogram('Dermatoglyphics'), true);
    assert.ok(isIsogram('isogram'), true);
    assert(isIsogram('isIsogram'), false);
    assert.ok(isIsogram(' subdermatoglyphic '), true);
    assert.ok(isIsogram('Subdermatoglyphic'), true);
    assert.ok(isIsogram('qwertyuiopasdfghjklzxcvbnm'), true);
  });

  it('same chars may not be adjacent', () => {
    assert(isIsogram('aba'), false);
  });

  it('same chars may not be same case', () => {
    assert(isIsogram('moOse'), false);
    assert(isIsogram('aSsert'), false);
  });

  it('an empty string is a valid isogram', () => {
    assert.ok(isIsogram(''), true);
    assert.ok(isIsogram('  '), true);
  });
});
