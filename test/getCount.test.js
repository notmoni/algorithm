const assert = require('assert');

const { describe } = require('mocha');
const { it } = require('mocha');

const getCount = require('../src/getVowelCount');

describe('getVowelCount', () => {
  it('should be defined', () => {
    assert.equal(getCount('abracadabra'), 5);
    assert.equal(getCount('my password is one two three'), 8);
    assert.equal(getCount('my pyx'), 0);
    assert.equal(getCount('May the Force be with you.'), 8);
  });
});
