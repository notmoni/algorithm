const assert = require('assert');

const { describe } = require('mocha');
const { it } = require('mocha');

const squareDigits = require('../src/squareDigits');

describe('SquareDigits', () => {
  it('should square the digits', () => {
    assert(squareDigits(9119), 811181);
  });
});
