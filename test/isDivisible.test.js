const assert = require('assert');

const { describe } = require('mocha');
const { it } = require('mocha');

const isDivisible = require('../src/isDivisible');

describe('isDivisible', () => {
  it('should be divisible', () => {
    assert.equal(isDivisible(12, 3, 4), true);
    assert.equal(isDivisible(48, 3, 4), true);
  });

  it('should not be divisible', () => {
    assert.equal(isDivisible(3, 3, 4), false);
    assert.equal(isDivisible(8, 3, 4), false);
  });
});
